# Runkit Plus Ultra

Run ESM, full featured CJS and even TS in Runkit and stick it to the man.

Todo: Add ability to run code in browser window, worker and service worker.

[RunKit Usage](https://runkit.com/hesygolu/60f26c0949b373001b27bedb)

## Usage:

```
const OmniLoader = require('runkit-plus-ultra')

const SCRIPT_SPECS = [
{ // ES Module
    path: '1.mjs', 
    code: `
  import cowsay from "cowsay"
  console.log(cowsay.say("I'm a cow"))
`
},
{ // CommonerJS Module
  path: '2.cjs', 
  code: `
  var cowsay = require("cowsay")
  console.log(cowsay.say("I don't say meow"))
`
}


const omniLoader = new OmniLoader({ scriptSpecs: SCRIPT_SPECS })
omniLoader.loadLatest('ansicolor')
omniLoader.saveCode()
omniLoader.runCode()
```

# About runkit

- The number in the node_modules directory is a number referring to the build date of the container that installed the package. Runkit is composed of many containers overlaid on eachother to achieve effictient update and installation behavior across all possible packages.
- If you get the uptime of the current container, you'll get a valid link to the node_modules directory as the current container is included first in the list of overlays.
