import OmniLoader from './index.js'

// todo: add dummy code with import and require statements
// todo: very the output on the tests... not merely just running them
test('adds 1 + 2 to equal 3', () => {
  const SCRIPT_SPECS = [{
    path: '1.mjs',
    code: 'console.log("yeet")'
  }, {
    path: '2.ts',
    code: `
    let fullName: string = 'Bob Bobbington'
    let age: number = 37
    let sentence: string = \`
      Hello, my name is \${fullName}.
      I'll be \${age + 1} years old next month.\`
    console.log(sentence)
    `
  }]
  const omniLoader = new OmniLoader({ scriptSpecs: SCRIPT_SPECS, auto: false })
  omniLoader.prepareBasics()
  omniLoader.prepareTSNode()
  omniLoader.saveCode()
  console.debug(omniLoader.runCode())
  console.debug(omniLoader)
  expect(1 + 2).toBe(3);
})
